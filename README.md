## flume 镜像使用步骤

*以下内容将指引你在 DataFoundry 上运行一个 flume agent 实例。*

### 准备配置文件

* 准备 `flume agent` 的配置文件，命名为`generic.conf`，配置文件内容示例：
```
agent.sources = s1
agent.channels = c1
agent.sinks = k1

agent.sources.s1.type = http
agent.sources.s1.handler = org.apache.flume.source.http.JSONHandler
agent.sources.s1.channels = c1
agent.sources.s1.bind = 0.0.0.0
agent.sources.s1.port = 1234

agent.channels.c1.type = memory

# Each sink's type must be defined
agent.sinks.k1.type = logger

# Specify the channel the sink should use
agent.sinks.k1.channel = c1
```
### 创建配置卷

* 进入 DataFoundry > 资源管理 > 配置卷 > 新建配置卷 > 添加配置文件；
* 选择配置文件 `generic.conf`，填写配置卷名称，如 flume，点击 `创建配置卷`。

### 部署镜像
* 进入 DataFoundry > 服务部署 > 创建服务；
* 填写服务名称，如 flume，点击 `下一步`；
* 填写容器名称，如 flume；
* 点击`选择镜像`>`平台公有镜像`，选择 `flume:latest`，点击 `使用`；
* 选择`自定义端口`，填写两组端口：
    <table>
        <tr>
            <td>容器端口</td>
            <td>服务端口</td>
        </tr>
        <tr>
            <td>1234</td>
            <td>1234</td>
        </tr>
        <tr>
            <td>34545</td>
            <td>34545</td>
        </tr>
    </table>

* 点击 `高级设置`，`挂载配置卷`；
* 从下拉列表中选择刚才创建的配置卷（本例中是 flume），并填写挂载路径 `/tmp/flume`，此挂载路径不可更改；
* 点击 `下一步`；
* 点击 `立即创建`。

*DataFoundry 会从公共镜像仓库拉取 flume 镜像并启动实例，拉取耗时视网络情况而定。*
